#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
sub_type_renderer.py : SubTypRenderer widget
-------------------------------------------------------------------------------
   Copyright (C) 2021 Oslandia <infos@oslandia.com>

   This file is a piece of free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.
   You should have received a copy of the GNU Library General Public
   License along with this library; if not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------
"""

from qgis.core import (
    QgsCategorizedSymbolRenderer,
    QgsGraduatedSymbolRenderer,
    QgsMarkerSymbol,
    QgsFillSymbol,
    QgsSingleSymbolRenderer,
)
from qgis.gui import (
    QgsCategorizedSymbolRendererWidget,
    QgsGraduatedSymbolRendererWidget,
    QgsSingleSymbolRendererWidget,
)
from qgis.PyQt.QtWidgets import QComboBox, QStackedWidget, QVBoxLayout, QWidget

SUBTYPERENDERER = [
    QgsSingleSymbolRenderer,
    QgsCategorizedSymbolRenderer,
    QgsGraduatedSymbolRenderer,
]


class SubTypeRenderer(QWidget):
    """SubTypeRenderer widget is used to stack
    single/categorized/classified renderer widget
    for point/linestring/polygon geometry
    """

    def __init__(self, layer, style, renderer, parent):
        super().__init__()
        self.__layout = QVBoxLayout(self)
        self.__sub_stack_widget = QStackedWidget()
        ssr_single_widget = QgsSingleSymbolRendererWidget(layer, style, renderer)
        self.__sub_stack_widget.addWidget(ssr_single_widget)

        self.__sub_combo = QComboBox()
        self.__sub_combo.addItem(self.tr("Single"))
        ssr_cat_widget = QgsCategorizedSymbolRendererWidget(layer, style, renderer)
        ssr_grad_widget = QgsGraduatedSymbolRendererWidget(layer, style, renderer)
        self.__sub_stack_widget.addWidget(ssr_cat_widget)
        self.__sub_stack_widget.addWidget(ssr_grad_widget)
        self.__sub_combo.addItem(self.tr("Categorized"))
        self.__sub_combo.addItem(self.tr("Graduated"))
        self.__sub_combo.currentIndexChanged[int].connect(self.__sub_stack_widget.setCurrentIndex)
        for i, subtype in enumerate(SUBTYPERENDERER):
            if isinstance(renderer, subtype):
                self.__sub_combo.setCurrentIndex(i)

        self.__layout.addWidget(self.__sub_combo)
        self.__layout.addWidget(self.__sub_stack_widget)

    def currentWidget(self):
        return self.__sub_stack_widget.currentWidget()
