#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
units.py : units management module
-------------------------------------------------------------------------------
   Copyright (C) 2018 Oslandia <infos@oslandia.com>

   This file is a piece of free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.
   You should have received a copy of the GNU Library General Public
   License along with this library; if not, see <http://www.gnu.org/licenses/>.
-------------------------------------------------------------------------------
"""

AVAILABLE_UNITS = {
    "-": ["-"],
    "m³/s": ["m³/s"],
    "kg/m³": [
        "kg/L",
        "g/L",
        "mg/L",
        "µg/L",
        "kg/m³",
        "g/m³",
        "mg/m³",
        "µg/m³"
    ],
    "Bq/m³": [
        "kg/m³",
        "GBq/L",
        "MBq/L",
        "kBq/L",
        "Bq/L",
        "mBq/L",
        "µBq/L",
        "GBq/m³",
        "MBq/m³",
        "kBq/m³",
        "Bq/m³",
        "mBq/m³",
        "µBq/m³",
    ],
    "Bq": [
        "µBq",
        "mBq",
        "Bq",
        "kBq",
        "MBq",
        "GBq"
    ],
    "m": ["nm", "µm", "mm", "cm", "m", "km"],
    "m³": ["m³", "mm³", "L"],
    "g": ["µg", "mg", "g", "kg", "Mg"],
    "g/g": ["µg/g", "mg/g", "g/g", "kg/g", "Mg/g", "µg/kg", "mg/kg", "g/kg", "kg/kg", "Mg/kg"],
    "N.m": ["N.mm", "N.m"],
    "Pa": ["Pa", "hPa", "kPa", "m", "mbar", "bar"],
    "°C": ["°C"],
    "Bq/kg": ["Bq/kg", "Bq/g", "Bq/mg", "Bq/µg", "µBq/kg", "mBq/kg", "kBq/kg", "MBq/kg", "GBq/kg"],
    "Gy": ["Gy"],
    "m/s": ["m/s", "m/an", "cm/j", "m/j", "kn"],
    "S/m": ["S/m", "mS/cm"],
}


def unit_conversion_factor(source_unit, target_unit, specific_activity=0.0):
    """ Return conversion factor between two units

    :param source_unit: source_unit
    :type source_unit: string
    :param target_unit: target_unit
    :type target_unit: string
    :param specific_activity: specific_activity
    :type specific_activity: float

    :return: factor
    :rtype: float
    """
    if source_unit == target_unit:
        return 1  # takes care of the adimentional case

    units_conversion_factors = {
        # Debit
        u"m³/s": {u"m³/s": 1.0},
        # Concentration
        u"kg/m³": {
            u"kg/L": 1e-3,
            u"g/L": 1.0,
            u"mg/L": 1e3,
            u"µg/L": 1e6,
            u"kg/m³": 1.0,
            u"g/m³": 1e3,
            u"mg/m³": 1e6,
            u"µg/m³": 1e9,
        },
        # Concentration (activity)
        u"Bq/m³": {
            u"kg/m³": 1.0 * 1 / specific_activity if specific_activity != 0.0 else 1,
            u"GBq/L": 1e-9 * 1e-3,
            u"MBq/L": 1e-6 * 1e-3,
            u"kBq/L": 1e-3 * 1e-3,
            u"Bq/L": 1e-3,
            u"mBq/L": 1e3 * 1e-3,
            u"µBq/L": 1e6 * 1e-3,
            u"GBq/m³": 1e-9,
            u"MBq/m³": 1e-6,
            u"kBq/m³": 1e-3,
            u"Bq/m³": 1.0,
            u"mBq/m³": 1e3,
            u"µBq/m³": 1e6,
        },
        # Activity
        u"Bq": {
            "µBq": 1e6,
            "mBq": 1e3,
            u"Bq": 1.0,
            "kBq": 1e-3,
            "MBq": 1e-6,
            "GBq": 1e-9
        },
        # Metric
        u"m": {u"nm": 1e9, u"µm": 1e6, u"mm": 1e3, u"cm": 1e2, u"m": 1.0, u"km": 1e-3},
        # Volume
        u"m³": {u"m³": 1.0, u"mm³": 1e9, u"L": 1e-3},
        # Weight
        u"g": {u"µg": 1e6, u"mg": 1e3, u"g": 1.0, u"kg": 1e-3, u"Mg": 1e-6},
        # Torque
        u"N.m": {u"N.mm": 1e3, u"N.m": 1.0},
        # Pressure
        u"Pa": {
            u"Pa": 1.0,
            u"hPa": 1e-2,
            u"kPa": 1e-3,
            u"mH20": 1 / 9810.0,  # Manometric measurements
            u"mbar": 1e-2,
            u"bar": 1e-5,
        },
        # Temperature
        u"°C": {u"°C": 1.0},
        # Specific activity
        u"Bq/kg": {
            u"Bq/kg": 1.0,
            u"Bq/g": 1e-3,
            u"Bq/mg": 1.0e-6,
            u"Bq/µg": 1.0e-9,
            u"µBq/kg": 1e6,
            u"mBq/kg": 1e3,
            u"kBq/kg": 1e-3,
            u"MBq/kg": 1e-6,
            u"GBq/kg": 1.0e-9,
        },
        # Dosimetry
        u"Gy": {u"Gy": 1.0},
        # Speed
        u"m/s": {
            u"m/s": 1.0,
            u"m/an": 365.25 * 24 * 3600,
            u"cm/j": 100.0 * 24 * 3600,
            u"m/j": 24 * 3600,
            u"kn": 3600 / 1852.0,  # Knot
        },
        # Conductivity
        u"S/m": {u"S/m": 1.0, u"mS/cm": 10.0},
        # Other
        u"-": {u"-": 1.0},
    }

    if source_unit not in units_conversion_factors:
        for key, value in units_conversion_factors.items():
            if source_unit in value:
                inverse_factor = 1.0 / value[source_unit]
                return inverse_factor * units_conversion_factors[key][target_unit]
        raise ValueError(
            f"'{target_unit}' does not match '{source_unit}' in the unit catalog..."
        )

    return units_conversion_factors[source_unit][target_unit]
