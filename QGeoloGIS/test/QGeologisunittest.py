# -*- coding: utf-8 -*-
"""QGeologis unit test

.. note:: This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.
"""

import json
import os
import shutil
import sys
import unittest
from distutils.spawn import find_executable

from QGeologis.config import SUBKEYS
from QGeologis.config_create_dialog import ConfigCreateDialog
from QGeologis.configure_plot_dialog import ConfigurePlotDialog
from QGeologis.data_selector import DataSelector
from QGeologis.data_stacker import DataStacker
from QGeologis.edit_plot import EditPlot
from QGeologis.main_dialog import TimeSeriesWrapper, WellLogViewWrapper
from QGeologis.qgeologis.imagery_data import ImageryDataItem
from QGeologis.qgeologis.plot_item import PlotItem
from QGeologis.qgeologis.stratigraphy import StratigraphyItem
from QGeologis.qgeologis.time_scale import TimeScaleItem
from QGeologis.qgeologis.z_scale import ZScaleItem
from QGeologis.qgis_plugin import QGeoloGISPlugin
from qgis.core import QgsApplication, QgsProject, QgsVectorDataProvider
from qgis.gui import QgsMapCanvas
from qgis.PyQt.QtCore import QObject, QPoint

TEST_DIR = os.path.join(os.path.dirname(__file__), os.path.splitext(__file__)[0])
SAMPLE_DIR = os.path.join(os.path.dirname(__file__), "..", "sample")


if os.path.exists(TEST_DIR):
    shutil.rmtree(TEST_DIR)
os.mkdir(TEST_DIR)

if os.path.exists(os.path.join(TEST_DIR, ".local")):
    shutil.rmtree(os.path.join(TEST_DIR, ".local"))
if os.path.exists(
    os.path.join(os.environ["HOME"], ".local", "share", "QGIS", "QGIS3", "profiles", "default")
):
    shutil.copytree(
        os.path.join(os.environ["HOME"], ".local", "share", "QGIS", "QGIS3", "profiles", "default"),
        os.path.join(TEST_DIR, ".local", "share", "QGIS", "QGIS3", "profiles", "default"),
    )
else:
    os.makedirs(os.path.join(TEST_DIR, ".local", "share", "QGIS", "QGIS3", "profiles", "default"))
if os.path.exists(os.path.join(os.environ["HOME"], ".pg_service.conf")):
    shutil.copy(
        os.path.join(os.environ["HOME"], ".pg_service.conf"),
        os.path.join(TEST_DIR, ".pg_service.conf"),
    )

os.environ["HOME"] = TEST_DIR

qgis_path = find_executable("qgis310")
if qgis_path is None:
    sys.exit("No qgis executable found, please add qgis folder in your PATH")

os.environ["QGIS_PREFIX_PATH"] = os.path.join(os.path.dirname(qgis_path), "..")
os.environ["LD_LIBRARY_PATH"] = os.path.join(os.path.dirname(qgis_path), "..", "lib")
os.environ["QGIS_PLUGIN_PATH"] = os.path.join(os.path.dirname(qgis_path), "..", "python", "plugins")

qgisApp = QgsApplication([], True)
qgisApp.initQgis()


class DummyInterface(QObject):
    def __init__(self, canvas):
        super().__init__()
        self.canvas = canvas
        self.active_layer = None

    def __getattr__(self, *args, **kwargs):
        def dummy(*args, **kwargs):
            return self

        return dummy

    def __iter__(self):
        return self

    def next(self):
        raise StopIteration

    def layers(self):
        # simulate iface.legendInterface().layers()
        return QgsProject.instance().mapLayers().values()

    def setActiveLayer(self, layer):
        self.active_layer = layer

    def activeLayer(self):
        return self.active_layer

    def newProject(self):
        QgsProject.instance().clear()

    def mapCanvas(self):
        return self.canvas


class QGeologisTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        """Run before all tests"""

        cls.bd_service = "qgeologistest"
        cls.canvas = QgsMapCanvas()
        cls.iface = DummyInterface(cls.canvas)
        cls.__qgeologis = QGeoloGISPlugin(cls.iface)
        cls.__qgeologis.initGui()

        print("=================================")
        print("==========QGeoloGISTest==========")
        print("=================================")

    @classmethod
    def setUp(cls):
        print("\n\n==========================================")

    @classmethod
    def tearDownClass(cls):
        """Run after all tests"""
        qgisApp.exit()

    def test_1loadQGeoloGIS(self):
        print("QGeologisTest::test_loadQGeoloGIS")
        print("---------------------------------------")

        self.__qgeologis = QGeoloGISPlugin(self.iface)
        self.__qgeologis.initGui()

        QgsProject.instance().read(os.path.join(SAMPLE_DIR, "project.qgs"))

        # avoid OpenStreetMap artifact
        lyr_list = []
        for lyr in QgsProject.instance().mapLayers().values():
            if isinstance(lyr.dataProvider(), QgsVectorDataProvider):
                self.assertTrue(lyr.isValid())
                lyr_list.append(lyr)

        self.assertTrue(len(lyr_list) == 30)

        print("\n test_loadQGeoloGIS SUCCESS")

    def test_2readAndCreateConfig(self):
        print("QGeologisTest::test_config")
        print("---------------------------------------")

        config, _ = QgsProject.instance().readEntry("QGeoloGIS", "config", "{}")
        config = json.loads(config)
        plugin_cfg = self.__qgeologis.get_config()

        self.assertTrue(config == plugin_cfg)
        self.assertTrue("Station" in list(config.keys())[0])
        lyr_station_id = list(config.keys())[0]

        expected_sections = [
            "layer_name",
            "id_column",
            "name_column",
            "ground_elevation",
            "stratigraphy_config",
            "log_measures",
            "timeseries",
            "imagery_data",
        ]
        for k, config_sections in config.items():
            for exp_section, section in zip(expected_sections, config_sections.keys()):
                self.assertTrue(exp_section == section)

        QgsProject.instance().removeEntry("QGeoloGIS", "config")
        self.__qgeologis.update_layer_config()
        self.assertTrue(self.__qgeologis.get_config() == {})

        # Define a layer
        dlg = ConfigurePlotDialog(QgsProject.instance().mapLayer(lyr_station_id), None)
        dlg._name.setText("Stations")
        dlg._id_column.setCurrentText("id")
        dlg._name_column.setCurrentText("name")
        dlg._elevation_column.setCurrentText("ground_altitude")
        self.__qgeologis.get_config()[lyr_station_id] = dlg.config()
        for k, config_sections in self.__qgeologis.get_config().items():
            for exp_section, section in zip(expected_sections, config_sections.keys()):
                self.assertTrue(exp_section == section)

        print("\n test_config SUCCESS")

    def test_3OpenPlotsDock(self):
        print("QGeologisTest::test_openPlotsDock")
        print("---------------------------------------")

        layer = QgsProject.instance().mapLayersByName("Stations")[0]
        self.iface.setActiveLayer(layer)

        self.__qgeologis.on_view_plots(self.__qgeologis.view_logs)

        self.assertTrue(len(self.__qgeologis._QGeoloGISPlugin__docks) == 1)

        self.__qgeologis.on_view_plots(self.__qgeologis.view_timeseries)

        self.assertTrue(len(self.__qgeologis._QGeoloGISPlugin__docks) == 2)

        self.__qgeologis.on_view_plots(self.__qgeologis.view_logs)

        self.assertTrue(len(self.__qgeologis._QGeoloGISPlugin__docks) == 1)

        self.__qgeologis.on_view_plots(self.__qgeologis.view_timeseries)

        self.assertTrue(len(self.__qgeologis._QGeoloGISPlugin__docks) == 0)

        print("\n test_openPlotsDock SUCCESS")

    def test_4createLogPlotConfig(self):
        print("QGeologisTest::test_LogView")
        print("---------------------------------------")

        QgsProject.instance().removeEntry("QGeoloGIS", "config")
        self.__qgeologis.update_layer_config()
        self.assertTrue(self.__qgeologis.get_config() == {})

        # Define a layer
        lyr_station_id = QgsProject.instance().mapLayersByName("Stations")[0].id()
        dlg = ConfigurePlotDialog(QgsProject.instance().mapLayer(lyr_station_id), None)
        dlg._name.setText("Stations")
        dlg._id_column.setCurrentText("id")
        dlg._name_column.setCurrentText("name")
        dlg._elevation_column.setCurrentText("ground_altitude")
        self.__qgeologis.get_config()[lyr_station_id] = dlg.config()
        layer = QgsProject.instance().mapLayersByName("Stations")[0]
        self.iface.setActiveLayer(layer)

        # Open dock
        self.__qgeologis.on_view_plots(self.__qgeologis.view_logs)

        main_dialog = self.__qgeologis._QGeoloGISPlugin__docks[0].widget()
        log_view = main_dialog._MainDialog__view

        # Check dialog type
        self.assertTrue(isinstance(log_view, WellLogViewWrapper))

        # Check there is no cells (no configuration nor selected feature)
        self.assertTrue(len(log_view._PlotView__cells) == 0)

        # Check selection works
        self.assertTrue(len(log_view._WellLogViewWrapper__features) == 0)
        main_dialog._MainDialog__layer.selectByIds([2])
        self.assertTrue(len(log_view._WellLogViewWrapper__features) == 1)

        # Check there is no cells (no selected feature)
        self.assertTrue(len(log_view._PlotView__cells) == 0)

        # Configure stratif cell
        dlg = ConfigCreateDialog(log_view)
        dlg._type.setCurrentIndex(3)
        dlg._source.setCurrentText("Stratigraphie")
        dlg._name.setText("Stratigraphie")
        dlg._feature_ref_column.setCurrentText("station_id")
        dlg._depth_from_column.setCurrentText("depth_from")
        dlg._depth_to_column.setCurrentText("depth_to")
        dlg._formation_code_column.setCurrentText("formation_code")
        dlg._formation_description_column.setCurrentText("formation_description")
        dlg._rock_code_column.setCurrentText("rock_code")
        dlg._rock_description_column.setCurrentText("rock_description")
        dlg._style_file.setCurrentText("stratigraphy_style.xml")
        config_type, plot_config = dlg.config()
        main_dialog._MainDialog__config.add_plot_config(config_type, plot_config)

        log_view.update_view()

        # Check plots appears (scale + stratif)
        self.assertTrue(len(log_view._PlotView__cells) == 2)
        self.assertTrue(isinstance(log_view._PlotView__cells[0][0], ZScaleItem))
        # Check auto-scale works
        self.assertTrue(log_view._PlotView__cells[0][0].min_depth() == 20.0)
        self.assertTrue(log_view._PlotView__cells[0][0].max_depth() == 120.0)

        self.assertTrue(isinstance(log_view._PlotView__cells[1][0], StratigraphyItem))
        stratif_item = log_view._PlotView__cells[1][0]

        # OpenForm
        # click out a formation
        self.assertFalse(
            stratif_item.form(
                QPoint(int(stratif_item.height() / 1.3), int(stratif_item.width() / 1.5))
            )
        )

        # click in a formation should open form
        # (but iface is not working in CLI mode, needed to except the attribute error)
        res = False
        try:
            stratif_item.form(
                QPoint(int(stratif_item.height() / 4.0), int(stratif_item.width() / 1.5))
            )
        except AttributeError:
            res = True
        self.assertTrue(res)

        # Check hide plot feature works
        self.assertFalse(log_view._PlotView__action_remove_cell.isEnabled())
        log_view.select_cell(1)
        self.assertTrue(log_view._PlotView__action_remove_cell.isEnabled())
        self.assertTrue(len(main_dialog._MainDialog__config.get_stratigraphy_plots()) == 1)
        log_view._PlotView__action_remove_cell.trigger()
        self.assertTrue(main_dialog._MainDialog__config.get_stratigraphy_plots() == [])

        # Check display plot feature works
        s = DataSelector(
            log_view,
            log_view._WellLogViewWrapper__features,
            main_dialog._MainDialog__config.get_stratigraphy_plots(False)
            + main_dialog._MainDialog__config.get_log_plots(False)
            + main_dialog._MainDialog__config.get_imageries(False),
            main_dialog._MainDialog__config,
        )
        s._DataSelector__list.setCurrentItem(s._DataSelector__list.item(0))
        for item in s.items():
            main_dialog._MainDialog__config.display_plot(item)
        log_view.update_view()

        self.assertTrue(len(main_dialog._MainDialog__config.get_stratigraphy_plots()) == 1)

        # Check remove plot configuration works
        s = DataSelector(
            log_view,
            log_view._WellLogViewWrapper__features,
            main_dialog._MainDialog__config.get_stratigraphy_plots()
            + main_dialog._MainDialog__config.get_log_plots()
            + main_dialog._MainDialog__config.get_imageries()
            + main_dialog._MainDialog__config.get_stratigraphy_plots(False)
            + main_dialog._MainDialog__config.get_log_plots(False)
            + main_dialog._MainDialog__config.get_imageries(False),
            main_dialog._MainDialog__config,
            removeMode=True,
        )
        s._DataSelector__list.setCurrentItem(s._DataSelector__list.item(0))
        for item in s.items():
            main_dialog._MainDialog__config.remove_plot_config(item)

        self.assertTrue(len(main_dialog._MainDialog__config.get_stratigraphy_plots()) == 0)

        # Load full configured project
        QgsProject.instance().read(os.path.join(SAMPLE_DIR, "project.qgs"))
        self.__qgeologis.update_layer_config()
        layer = QgsProject.instance().mapLayersByName("Stations")[0]
        self.iface.setActiveLayer(layer)
        self.__qgeologis.on_view_plots(self.__qgeologis.view_logs)
        main_dialog = self.__qgeologis._QGeoloGISPlugin__docks[0].widget()
        main_dialog._MainDialog__layer.selectByIds([2])
        log_view = main_dialog._MainDialog__view
        self.assertTrue(len(log_view._PlotView__cells) == 8)
        expected_types = [
            ZScaleItem,
            StratigraphyItem,
            PlotItem,
            PlotItem,
            PlotItem,
            PlotItem,
            PlotItem,
            ImageryDataItem,
        ]
        for (plot, _, _), exp_type in zip(log_view._PlotView__cells, expected_types):
            self.assertTrue(isinstance(plot, exp_type))

        # Edit plot
        log_view.select_cell(3)
        item, legend, _ = log_view._PlotView__cells[3]
        layer_config = main_dialog._MainDialog__config
        for data_type in SUBKEYS:
            for conf in layer_config._LayerConfig__config[data_type]:
                if conf["source"] == item.layer().id():
                    if "uom" in conf.keys():
                        uom = conf["uom"]
                        displayed_uom = conf["displayed_uom"]
                    elif "uom_column" in conf.keys():
                        uom = "Categorical"
                        displayed_uom = None
        dlg = EditPlot(item, legend, uom, displayed_uom=displayed_uom)
        self.assertTrue(
            dlg.plot_config()
            == (False, "m/s", -36319.551866677604, 399515.0705334536, None, 150, 10)
        )
        dlg._log_scale_chk.setChecked(True)
        dlg._min_value.setText("100")
        dlg._max_value.setText("1000")
        dlg._uncertainty_cb.setCurrentText("measures")
        dlg._unit_cb.setCurrentText("m/an")
        dlg._plot_size.setText("200")
        dlg._font_size_sb.setValue(15)
        self.assertTrue(dlg.plot_config() == (True, "m/an", 100.0, 1000.0, "measures", 200, 15))

        print("\n test_logView SUCCESS")

    def test_5createTimeSeriesPlotConfig(self):
        print("QGeologisTest::test_TimeSeriesView")
        print("---------------------------------------")

        QgsProject.instance().removeEntry("QGeoloGIS", "config")
        self.__qgeologis.update_layer_config()
        self.assertTrue(self.__qgeologis.get_config() == {})

        # Define a layer
        lyr_station_id = QgsProject.instance().mapLayersByName("Stations")[0].id()
        dlg = ConfigurePlotDialog(QgsProject.instance().mapLayer(lyr_station_id), None)
        dlg._name.setText("Stations")
        dlg._id_column.setCurrentText("id")
        dlg._name_column.setCurrentText("name")
        dlg._elevation_column.setCurrentText("ground_altitude")
        self.__qgeologis.get_config()[lyr_station_id] = dlg.config()
        layer = QgsProject.instance().mapLayersByName("Stations")[0]
        layer.selectByIds([])
        self.iface.setActiveLayer(layer)

        # Open dock
        self.__qgeologis.on_view_plots(self.__qgeologis.view_timeseries)

        main_dialog = self.__qgeologis._QGeoloGISPlugin__docks[0].widget()
        TS_view = main_dialog._MainDialog__view

        # Check dialog type
        self.assertTrue(isinstance(TS_view, TimeSeriesWrapper))

        # Check there is no cells (no configuration nor selected feature)
        self.assertTrue(len(TS_view._PlotView__cells) == 0)

        # Check selection works
        self.assertTrue(len(TS_view._TimeSeriesWrapper__features) == 0)
        main_dialog._MainDialog__layer.selectByIds([1])
        self.assertTrue(len(TS_view._TimeSeriesWrapper__features) == 1)

        # Check there is no cells (no selected feature)
        self.assertTrue(len(TS_view._PlotView__cells) == 0)

        # Configure chemical cell
        dlg = ConfigCreateDialog(TS_view, timeseries=True)
        dlg._type.setCurrentIndex(0)
        dlg._source.setCurrentText("Analyse chimique")
        dlg._name.setText("Analyse chimique")
        dlg._feature_ref_column.setCurrentText("station_id")
        dlg._uom_type_instantaneous_cb.setCurrentText("Concentration")
        dlg._uom_instantaneous_cb.setCurrentText("kg/m³")
        dlg._event_column.setCurrentText("measure_epoch")
        dlg._value_instantaneous_column.setCurrentText("measure_value")
        dlg._cat_instantaneous_column.setCurrentText("chemical_element")
        dlg._uncertainty_instantaneous_column.setCurrentText("measure_uncertainty")
        config_type, plot_config = dlg.config()
        main_dialog._MainDialog__config.add_plot_config(config_type, plot_config)

        TS_view.update_view()
        # Still no cells because we defined a categorical data, need to be add manually
        self.assertTrue(len(TS_view._PlotView__cells) == 0)

        # Check display plot feature works
        s = DataSelector(
            TS_view,
            TS_view._TimeSeriesWrapper__features,
            main_dialog._MainDialog__config.get_timeseries(False),
            main_dialog._MainDialog__config,
        )
        s._DataSelector__list.setCurrentItem(s._DataSelector__list.item(0))
        s._DataSelector__sub_selection_combo.setCurrentText("Cu")
        for item in s.items():
            main_dialog._MainDialog__config.display_plot(item)
        TS_view.update_view()

        self.assertTrue(len(main_dialog._MainDialog__config.get_timeseries(False)) == 1)

        # Check plots appears (scale + chemical)
        self.assertTrue(len(TS_view._PlotView__cells) == 2)

        self.assertTrue(isinstance(TS_view._PlotView__cells[1][0], TimeScaleItem))
        # Check auto-scale works
        self.assertTrue(TS_view._PlotView__cells[0][0].min_depth() == 1546300800.0)
        self.assertTrue(TS_view._PlotView__cells[0][0].max_depth() == 1556668800.0)

        self.assertTrue(isinstance(TS_view._PlotView__cells[0][0], PlotItem))
        chemical_item = TS_view._PlotView__cells[0][0]

        # fake paint
        chemical_item.test_build_data()

        TS_view.update_view()
        # OpenForm
        # click out a formation
        self.assertFalse(
            chemical_item.form(
                QPoint(int(chemical_item.height() / 1.3), int(chemical_item.width() / 1.5))
            )
        )

        # click in a formation should open form
        # (but iface is not working in CLI mode, needed to except the attribute error)
        res = False
        try:
            chemical_item.form(QPoint(236, 95))
        except AttributeError:
            res = True
        self.assertTrue(res)

        # Check hide plot feature works
        self.assertFalse(TS_view._PlotView__action_remove_cell.isEnabled())
        TS_view.select_cell(0)
        self.assertTrue(TS_view._PlotView__action_remove_cell.isEnabled())
        self.assertTrue(len(main_dialog._MainDialog__config.get_timeseries()) == 1)
        TS_view._PlotView__action_remove_cell.trigger()
        self.assertTrue(main_dialog._MainDialog__config.get_timeseries() == [])

        # Check remove plot configuration works
        s = DataSelector(
            TS_view,
            TS_view._TimeSeriesWrapper__features,
            main_dialog._MainDialog__config.get_stratigraphy_plots()
            + main_dialog._MainDialog__config.get_log_plots()
            + main_dialog._MainDialog__config.get_imageries()
            + main_dialog._MainDialog__config.get_stratigraphy_plots(False)
            + main_dialog._MainDialog__config.get_log_plots(False)
            + main_dialog._MainDialog__config.get_imageries(False),
            main_dialog._MainDialog__config,
            removeMode=True,
        )
        s._DataSelector__list.setCurrentItem(s._DataSelector__list.item(0))
        for item in s.items():
            main_dialog._MainDialog__config.remove_plot_config(item)

        self.assertTrue(len(main_dialog._MainDialog__config.get_timeseries()) == 0)

        # Load full configured project
        QgsProject.instance().read(os.path.join(SAMPLE_DIR, "project.qgs"))
        self.__qgeologis.update_layer_config()
        layer = QgsProject.instance().mapLayersByName("Stations")[0]
        self.iface.setActiveLayer(layer)
        self.__qgeologis.on_view_plots(self.__qgeologis.view_timeseries)
        main_dialog = self.__qgeologis._QGeoloGISPlugin__docks[0].widget()
        main_dialog._MainDialog__layer.selectByIds([1])
        TS_view = main_dialog._MainDialog__view
        self.assertTrue(len(TS_view._PlotView__cells) == 6)
        expected_types = [PlotItem, PlotItem, PlotItem, PlotItem, PlotItem, TimeScaleItem]
        for (plot, _, _), exp_type in zip(TS_view._PlotView__cells, expected_types):
            self.assertTrue(isinstance(plot, exp_type))

        # Edit plot
        TS_view.select_cell(3)
        item, legend, _ = TS_view._PlotView__cells[3]
        layer_config = main_dialog._MainDialog__config
        for data_type in SUBKEYS:
            for conf in layer_config._LayerConfig__config[data_type]:
                if conf["source"] == item.layer().id():
                    if "uom" in conf.keys():
                        uom = conf["uom"]
                        displayed_uom = conf["displayed_uom"]
                    elif "uom_column" in conf.keys():
                        uom = "Categorical"
                        displayed_uom = None
        dlg = EditPlot(item, legend, uom, displayed_uom=displayed_uom)
        self.assertTrue(
            dlg.plot_config()
            == (False, "m3/s", -0.00019284502799927773, 0.002124109121877687, None, 150, 10)
        )
        dlg._log_scale_chk.setChecked(True)
        dlg._min_value.setText("100")
        dlg._max_value.setText("1000")
        dlg._uncertainty_cb.setCurrentText("None")
        dlg._unit_cb.setCurrentText("m3/s")
        dlg._plot_size.setText("200")
        dlg._font_size_sb.setValue(15)
        self.assertTrue(dlg.plot_config() == (True, "m3/s", 100.0, 1000.0, None, 200, 15))

        # Stack plot
        TS_view.select_cell(len(TS_view._PlotView__cells) - 2)
        item, legend, _ = TS_view.selected_cell()
        s = DataStacker(
            TS_view,
            TS_view._TimeSeriesWrapper__features,
            main_dialog._MainDialog__config.get_layer_config_from_layer_id(item.layer().id()),
            main_dialog._MainDialog__config,
        )
        self.assertTrue(len(s._DataStacker__available) == 4)
        s._available_list.setCurrentItem(s._available_list.item(1))
        s._add_button.clicked.emit()
        self.assertTrue(s._stacked_list.count() == 1)
        for cfg in main_dialog._MainDialog__config.get_timeseries():
            if cfg["name"] == "Analyse chimique":
                self.assertTrue(len(list(cfg["stacked"].values())) == 1)

        print("\n test_timeSeriesView SUCCESS")


if __name__ == "__main__":
    unittest.main()
