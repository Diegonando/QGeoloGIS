<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>@default</name>
    <message>
        <location filename="../qgis_plugin.py" line="235"/>
        <source>View log plots</source>
        <translation>Afficher les logs de forages</translation>
    </message>
    <message>
        <location filename="../qgis_plugin.py" line="241"/>
        <source>View timeseries</source>
        <translation>Afficher les séries temporelles</translation>
    </message>
    <message>
        <location filename="../qgis_plugin.py" line="154"/>
        <source>Import configuration</source>
        <translation>Importer une configuration</translation>
    </message>
    <message>
        <location filename="../qgis_plugin.py" line="155"/>
        <source>Export configuration to ...</source>
        <translation>Exporter une configuration ...</translation>
    </message>
    <message>
        <location filename="../qgis_plugin.py" line="235"/>
        <source>Well Logs</source>
        <translation>Logs de forages</translation>
    </message>
    <message>
        <location filename="../qgis_plugin.py" line="241"/>
        <source>Timeseries</source>
        <translation>Séries temporelles</translation>
    </message>
    <message>
        <location filename="../qgis_plugin.py" line="280"/>
        <source>Choose a configuration file to import</source>
        <translation>Choisir un fichier de configuration à importer</translation>
    </message>
    <message>
        <location filename="../qgis_plugin.py" line="290"/>
        <source>Overwrite existing layers</source>
        <translation>Ecraser les couches existantes</translation>
    </message>
    <message>
        <location filename="../qgis_plugin.py" line="313"/>
        <source>Export the configuration to ...</source>
        <translation>Exporter la configuration vers ...</translation>
    </message>
</context>
<context>
    <name>Axis</name>
    <message>
        <location filename="../qgeologis/plot_view.py" line="612"/>
        <source>Elevation</source>
        <translation>Altitude</translation>
    </message>
    <message>
        <location filename="../qgeologis/plot_view.py" line="614"/>
        <source>Depth</source>
        <translation>Profondeur</translation>
    </message>
    <message>
        <location filename="../qgeologis/plot_view.py" line="616"/>
        <source>Time</source>
        <translation>Temps</translation>
    </message>
    <message>
        <location filename="../qgeologis/plot_view.py" line="956"/>
        <source>Stack a data to the selected data cell</source>
        <translation>Empiler une donnée supplémentaire dans la cellule</translation>
    </message>
    <message>
        <location filename="../qgeologis/plot_view.py" line="958"/>
        <source>Add a data cell</source>
        <translation>Afficher une visualisation</translation>
    </message>
</context>
<context>
    <name>ConfigCreateDialog</name>
    <message>
        <location filename="../config_create_dialog.py" line="32"/>
        <source>Instantaneous temporal measures</source>
        <translation>Mesures temporelles instantanées</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.py" line="33"/>
        <source>Cumulative temporal measures</source>
        <translation>Mesures temporelles cumulatives</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.py" line="37"/>
        <source>Instantaneous log measures</source>
        <translation>Mesures instantanées de forages</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.py" line="38"/>
        <source>Cumulative log measures</source>
        <translation>Mesures cumulatives de forages</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.py" line="39"/>
        <source>Continuous log measures</source>
        <translation>Mesures continues de forages</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.py" line="40"/>
        <source>Stratigraphic measures</source>
        <translation>Mesures stratigraphiques</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.py" line="63"/>
        <source>Debit</source>
        <translation>Débit</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.py" line="67"/>
        <source>Volume</source>
        <translation>Volume</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.py" line="68"/>
        <source>Length</source>
        <translation>Longueur</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.py" line="69"/>
        <source>Weight</source>
        <translation>Poids</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.py" line="70"/>
        <source>Torque</source>
        <translation>Moment de force</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.py" line="71"/>
        <source>Pressure</source>
        <translation>Pression</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.py" line="72"/>
        <source>Temperature</source>
        <translation>Température</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.py" line="74"/>
        <source>Radioactivity</source>
        <translation>Radioactivé</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.py" line="75"/>
        <source>Speed</source>
        <translation>Vitesse</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.py" line="76"/>
        <source>pH</source>
        <translation>pH</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.py" line="77"/>
        <source>Conductivity</source>
        <translation>Conductivité</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.py" line="62"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.py" line="64"/>
        <source>Concentration</source>
        <translation>Concentration</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.py" line="223"/>
        <source>Activity concentration</source>
        <translation>Concentration (activité)</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.py" line="73"/>
        <source>Dosimetry</source>
        <translation>Dosimétrie</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.py" line="66"/>
        <source>Activity</source>
        <translation>Activité</translation>
    </message>
</context>
<context>
    <name>ConfigurePlotDialog</name>
    <message>
        <location filename="../configure_plot_dialog.py" line="40"/>
        <source>
Do you want to configure one ?</source>
        <translation>
Voulez-vous en configurer une ?</translation>
    </message>
    <message>
        <location filename="../configure_plot_dialog.py" line="44"/>
        <source>Configure a plot layer</source>
        <translation>Configurer une visualisation de couche</translation>
    </message>
    <message>
        <location filename="../configure_plot_dialog.py" line="40"/>
        <source>There is not plot configured for layer</source>
        <translation>Aucune visualisation configurée pour cette couche</translation>
    </message>
</context>
<context>
    <name>DataInterface</name>
    <message>
        <location filename="../qgeologis/data_interface.py" line="41"/>
        <source>DataInterface is an abstract class, get_x_values() must be defined</source>
        <translation>DataInterface est une classe abstraite, get_x_values() doit être défini</translation>
    </message>
    <message>
        <location filename="../qgeologis/data_interface.py" line="45"/>
        <source>DataInterface is an abstract class, get_y_values() must be defined</source>
        <translation>DataInterface est une classe abstraite, get_y_values() doit être défini</translation>
    </message>
    <message>
        <location filename="../qgeologis/data_interface.py" line="53"/>
        <source>DataInterface is an abstract class, get_x_min() must be defined</source>
        <translation>DataInterface est une classe abstraite, get_x_min() doit être défini</translation>
    </message>
    <message>
        <location filename="../qgeologis/data_interface.py" line="57"/>
        <source>DataInterface is an abstract class, get_y_min() must be defined</source>
        <translation>DataInterface est une classe abstraite, get_y_min() doit être défini</translation>
    </message>
    <message>
        <location filename="../qgeologis/data_interface.py" line="69"/>
        <source>DataInterface is an abstract class, get_y_max() must be defined</source>
        <translation>DataInterface est une classe abstraite, get_y_max() doit être défini</translation>
    </message>
    <message>
        <location filename="../qgeologis/data_interface.py" line="77"/>
        <source>DataInterface is an abstract class, get_layer() must be defined</source>
        <translation>DataInterface est une classe abstraite, get_layer() doit être défini</translation>
    </message>
</context>
<context>
    <name>DataSelector</name>
    <message>
        <location filename="../data_selector.py" line="78"/>
        <source>Sub selection</source>
        <translation>Sous-sélection</translation>
    </message>
    <message>
        <location filename="../data_selector.py" line="95"/>
        <source>Remove layer configuration</source>
        <translation>Retirer toute la configuration</translation>
    </message>
    <message>
        <location filename="../data_selector.py" line="103"/>
        <source>Choose the data to add</source>
        <translation>Choisir une donnée à ajouter à la configuration</translation>
    </message>
    <message>
        <location filename="../data_selector.py" line="105"/>
        <source>Choose the data to remove</source>
        <translation>Choisir une donnée à retirer de la configuration</translation>
    </message>
    <message>
        <location filename="../data_selector.py" line="116"/>
        <source>Unvalid configuration, please remove configuration</source>
        <translation>Configuration invalide, veuillez retirer la configuration</translation>
    </message>
    <message>
        <location filename="../data_selector.py" line="182"/>
        <source>Categorical plot should be added separately</source>
        <translation>L&apos;ajout de graphique avec une sous-sélection doit être fait séparement</translation>
    </message>
    <message>
        <location filename="../data_selector.py" line="226"/>
        <source>No feature found in layer</source>
        <translation>La couche ne contient aucune entité</translation>
    </message>
    <message>
        <location filename="../data_selector.py" line="242"/>
        <source>Elevation</source>
        <translation>Altitude</translation>
    </message>
    <message>
        <location filename="../data_selector.py" line="244"/>
        <source>Depth</source>
        <translation>Profondeur</translation>
    </message>
    <message>
        <location filename="../data_selector.py" line="246"/>
        <source>Time</source>
        <translation>Temps</translation>
    </message>
</context>
<context>
    <name>Dialog</name>
    <message>
        <location filename="../config_create_dialog.ui" line="14"/>
        <source>Add a plot configuration</source>
        <translation>Ajouter une configuation de visualisation</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.ui" line="22"/>
        <source>Source</source>
        <translation>Source</translation>
    </message>
    <message>
        <location filename="../configure_plot_dialog.ui" line="59"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../configure_plot_dialog.ui" line="66"/>
        <source>New plot</source>
        <translation>Nouveau graphique</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.ui" line="46"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.ui" line="56"/>
        <source>Feature reference column</source>
        <translation>Colonne de référence d&apos;entité</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.ui" line="302"/>
        <source>Unity of measure</source>
        <translation>Unité de mesure</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.ui" line="101"/>
        <source>Event column</source>
        <translation>Colonne d&apos;évènement</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.ui" line="337"/>
        <source>Value column</source>
        <translation>Colonne de valeur</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.ui" line="231"/>
        <source>Categorical column</source>
        <translation>Colonne de catégorie</translation>
    </message>
    <message>
        <location filename="../edit_plot.ui" line="70"/>
        <source>None</source>
        <translation>None</translation>
    </message>
    <message>
        <location filename="../edit_plot.ui" line="62"/>
        <source>Uncertainty column</source>
        <translation>Colonne d&apos;intervalle de confiance</translation>
    </message>
    <message>
        <location filename="../edit_plot.ui" line="62"/>
        <source>Min value</source>
        <translation>Valeur min</translation>
    </message>
    <message>
        <location filename="../edit_plot.ui" line="76"/>
        <source>Max value</source>
        <translation>Valeur max</translation>
    </message>
    <message>
        <location filename="../edit_plot.ui" line="45"/>
        <source>Logarithmic scale</source>
        <translation>Echelle logarithmique</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.ui" line="317"/>
        <source>Start measure column</source>
        <translation>Colonne de début de la mesure</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.ui" line="211"/>
        <source>End mesure column</source>
        <translation>Colonne de fin de la mesure</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.ui" line="327"/>
        <source>Interval column</source>
        <translation>Colonne d&apos;intervalle</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.ui" line="375"/>
        <source>Depth from column</source>
        <translation>Profondeur depuis</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.ui" line="385"/>
        <source>Depth to column</source>
        <translation>Profondeur jusqu&apos;à</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.ui" line="395"/>
        <source>Formation code column</source>
        <translation>Colonne de code de formation</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.ui" line="405"/>
        <source>Rock code column</source>
        <translation>Colonne de code de roche</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.ui" line="415"/>
        <source>Formation description column</source>
        <translation>Colonne de description des formations</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.ui" line="425"/>
        <source>Rock description column</source>
        <translation>Colonne de description des roches</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.ui" line="435"/>
        <source>Style</source>
        <translation>Style</translation>
    </message>
    <message>
        <location filename="../configure_plot_dialog.ui" line="14"/>
        <source>Configure a plot for this layer</source>
        <translation>Configurer une visualisation pour cette couche</translation>
    </message>
    <message>
        <location filename="../configure_plot_dialog.ui" line="26"/>
        <source>There is not plot configured for this layer.
Do you want to configure one ?</source>
        <translation>Il n&apos;existe pas de visualisation configurée pour cette couche.
Voulez-vous en configurer une ?</translation>
    </message>
    <message>
        <location filename="../configure_plot_dialog.ui" line="73"/>
        <source>Id column</source>
        <translation>Colonne id</translation>
    </message>
    <message>
        <location filename="../configure_plot_dialog.ui" line="83"/>
        <source>Name column</source>
        <translation>Colonne Nom</translation>
    </message>
    <message>
        <location filename="../configure_plot_dialog.ui" line="93"/>
        <source>Ground elevation column</source>
        <translation>Colonne d&apos;élévation</translation>
    </message>
    <message>
        <location filename="../configure_plot_dialog.ui" line="93"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../edit_plot.ui" line="14"/>
        <source>Plot edition</source>
        <translation>Edition de la visualisation</translation>
    </message>
    <message>
        <location filename="../edit_plot.ui" line="78"/>
        <source>Plot size (px)</source>
        <translation>Taille de la visualisation (px)</translation>
    </message>
    <message>
        <location filename="../edit_plot.ui" line="52"/>
        <source>Unit</source>
        <translation>Unité</translation>
    </message>
    <message>
        <location filename="../data_stacker.ui" line="14"/>
        <source>Stacked plot management</source>
        <translation>Configuration de colonnes associées</translation>
    </message>
    <message>
        <location filename="../data_stacker.ui" line="24"/>
        <source>Available plots</source>
        <translation>Colonnes disponibles</translation>
    </message>
    <message>
        <location filename="../data_stacker.ui" line="67"/>
        <source>Stacked plots</source>
        <translation>Colonnes empilées</translation>
    </message>
    <message>
        <location filename="../edit_plot.ui" line="92"/>
        <source>Title font size (px)</source>
        <translation>Taille de titre (px)</translation>
    </message>
    <message>
        <location filename="../config_create_dialog.ui" line="263"/>
        <source>Specific activity column</source>
        <translation>Colonne d&apos;activité spécifique</translation>
    </message>
</context>
<context>
    <name>FeatureData</name>
    <message>
        <location filename="../qgeologis/data_interface.py" line="732"/>
        <source>Define either x_values or x_start / x_delta</source>
        <translation>x_values ou x_start / x_delta doivent être définis</translation>
    </message>
    <message>
        <location filename="../qgeologis/data_interface.py" line="736"/>
        <source>Both x_start and x_delta must be defined</source>
        <translation>x_start et x_delta doivent être défini</translation>
    </message>
    <message>
        <location filename="../qgeologis/data_interface.py" line="742"/>
        <source>More than one feature, but only one starting value, define x_start_fieldname</source>
        <translation>Plus d&apos;une entité chargé, mais seulement une valeur de départ chargée, veuillez définir x_start_fieldname</translation>
    </message>
    <message>
        <location filename="../qgeologis/data_interface.py" line="748"/>
        <source>More than one feature, but only one delta value, define x_delta_fieldname</source>
        <translation>Plus d&apos;une entité chargé, mais seulement une valeur de delta chargée, veuillez définir x_start_fieldname</translation>
    </message>
    <message>
        <location filename="../qgeologis/data_interface.py" line="871"/>
        <source>Unsupported data format:</source>
        <translation>Format de données non supporté:</translation>
    </message>
    <message>
        <location filename="../qgeologis/data_interface.py" line="892"/>
        <source>Overlap in data around feature #</source>
        <translation>Les données se superposent à l&apos;entité #</translation>
    </message>
</context>
<context>
    <name>MainDialog</name>
    <message>
        <location filename="../main_dialog.py" line="832"/>
        <source>Invalid plot_type</source>
        <translation>Type de graphique invalide</translation>
    </message>
</context>
<context>
    <name>PlotItem</name>
    <message>
        <location filename="../qgeologis/plot_item.py" line="322"/>
        <source>X and Y array has different length : </source>
        <translation>Les listes X et Y sont de tailles différentes : </translation>
    </message>
    <message>
        <location filename="../qgeologis/plot_item.py" line="330"/>
        <source>Y and uncertainty values array has different length : </source>
        <translation>Les listes de valeurs Y et d&apos;intervalles de confiance ne sont pas de mêmes longueurs : </translation>
    </message>
    <message>
        <location filename="../qgeologis/plot_item.py" line="2087"/>
        <source>Wrong bisect output, the click is probably outside the data area.</source>
        <translation>Mauvaise sortie de bisect, le clic est probablement en dehors de l&apos;étendue de la donnée.</translation>
    </message>
    <message>
        <location filename="../edit_plot.ui" line="120"/>
        <source>Time:</source>
        <translation>Temps:</translation>
    </message>
    <message>
        <location filename="../edit_plot.ui" line="120"/>
        <source>Value:</source>
        <translation>Valeur:</translation>
    </message>
    <message>
        <location filename="../edit_plot.ui" line="120"/>
        <source>Depth:</source>
        <translation>Profondeur:</translation>
    </message>
    <message>
        <location filename="../edit_plot.ui" line="120"/>
        <source>Elevation:</source>
        <translation>Altitude:</translation>
    </message>
    <message>
        <location filename="../qgeologis/plot_item.py" line="1984"/>
        <source>Time: </source>
        <translation>Temps: </translation>
    </message>
    <message>
        <location filename="../qgeologis/plot_item.py" line="2006"/>
        <source>Value: </source>
        <translation>Valeur: </translation>
    </message>
    <message>
        <location filename="../qgeologis/plot_item.py" line="2006"/>
        <source>Depth: </source>
        <translation>Profondeur: </translation>
    </message>
    <message>
        <location filename="../qgeologis/plot_item.py" line="2006"/>
        <source>Elevation: </source>
        <translation>Altitude: </translation>
    </message>
</context>
<context>
    <name>PlotView</name>
    <message>
        <location filename="../qgeologis/plot_view.py" line="456"/>
        <source>Move up</source>
        <translation>Monter</translation>
    </message>
    <message>
        <location filename="../qgeologis/plot_view.py" line="459"/>
        <source>Move down</source>
        <translation>Descendre</translation>
    </message>
    <message>
        <location filename="../qgeologis/plot_view.py" line="463"/>
        <source>Move left</source>
        <translation>Placer à gauche</translation>
    </message>
    <message>
        <location filename="../qgeologis/plot_view.py" line="466"/>
        <source>Move right</source>
        <translation>Placer à droite</translation>
    </message>
    <message>
        <location filename="../qgeologis/plot_view.py" line="494"/>
        <source>Edit style</source>
        <translation>Editer le style</translation>
    </message>
    <message>
        <location filename="../qgeologis/plot_view.py" line="1021"/>
        <source>Add a data cell</source>
        <translation>Afficher une visualisation</translation>
    </message>
    <message>
        <location filename="../qgeologis/plot_view.py" line="504"/>
        <source>Remove the cell</source>
        <translation>Cacher une visualisation</translation>
    </message>
    <message>
        <location filename="../qgeologis/plot_view.py" line="1021"/>
        <source>Stack a data to the selected data cell</source>
        <translation>Empiler une donnée supplémentaire dans la cellule</translation>
    </message>
    <message>
        <location filename="../qgeologis/plot_view.py" line="612"/>
        <source>Elevation</source>
        <translation>Altitude</translation>
    </message>
    <message>
        <location filename="../qgeologis/plot_view.py" line="614"/>
        <source>Depth</source>
        <translation>Profondeur</translation>
    </message>
    <message>
        <location filename="../qgeologis/plot_view.py" line="616"/>
        <source>Time</source>
        <translation>Temps</translation>
    </message>
    <message>
        <location filename="../qgeologis/plot_view.py" line="476"/>
        <source>Reset y range</source>
        <translation>Réinitialiser l&apos;étendue en y</translation>
    </message>
    <message>
        <location filename="../qgeologis/plot_view.py" line="483"/>
        <source>Set y range</source>
        <translation>Définir l&apos;étendue en y</translation>
    </message>
    <message>
        <location filename="../qgeologis/plot_view.py" line="489"/>
        <source>Display legend</source>
        <translation>Afficher la légende</translation>
    </message>
    <message>
        <location filename="../qgeologis/plot_view.py" line="1143"/>
        <source>Legend</source>
        <translation>Légende</translation>
    </message>
</context>
<context>
    <name>StratigraphyItem</name>
    <message>
        <location filename="../qgeologis/stratigraphy.py" line="386"/>
        <source>Depth: </source>
        <translation>Profondeur: </translation>
    </message>
    <message>
        <location filename="../qgeologis/stratigraphy.py" line="386"/>
        <source>Rock: </source>
        <translation>Roche: </translation>
    </message>
    <message>
        <location filename="../qgeologis/stratigraphy.py" line="386"/>
        <source>Elevation: </source>
        <translation>Altitude: </translation>
    </message>
    <message>
        <location filename="../qgeologis/stratigraphy.py" line="386"/>
        <source>Formation: </source>
        <translation>Formation: </translation>
    </message>
</context>
<context>
    <name>StratigraphyStyleDialog</name>
    <message>
        <location filename="../qgeologis/stratigraphy.py" line="481"/>
        <source>Load style</source>
        <translation>Charger un style</translation>
    </message>
    <message>
        <location filename="../qgeologis/stratigraphy.py" line="482"/>
        <source>Save style</source>
        <translation>Sauver un style</translation>
    </message>
    <message>
        <location filename="../qgeologis/stratigraphy.py" line="491"/>
        <source>Unique symbol</source>
        <translation>Symbole unique</translation>
    </message>
    <message>
        <location filename="../qgeologis/stratigraphy.py" line="493"/>
        <source>Categorized</source>
        <translation>Catégorisé</translation>
    </message>
    <message>
        <location filename="../qgeologis/stratigraphy.py" line="498"/>
        <source>Graduated</source>
        <translation>Gradué</translation>
    </message>
    <message>
        <location filename="../qgeologis/stratigraphy.py" line="536"/>
        <source>Style file to save</source>
        <translation>Ficher de style à sauver</translation>
    </message>
    <message>
        <location filename="../qgeologis/stratigraphy.py" line="548"/>
        <source>Style file to load</source>
        <translation>Fichier de style à charger</translation>
    </message>
    <message>
        <location filename="../qgeologis/stratigraphy.py" line="492"/>
        <source>Rules set</source>
        <translation>Ensemble de règles</translation>
    </message>
</context>
<context>
    <name>SubTypeRenderer</name>
    <message>
        <location filename="../qgeologis/sub_type_renderer.py" line="57"/>
        <source>Single</source>
        <translation>Symbole unique</translation>
    </message>
    <message>
        <location filename="../qgeologis/sub_type_renderer.py" line="62"/>
        <source>Categorized</source>
        <translation>Catégorisé</translation>
    </message>
    <message>
        <location filename="../qgeologis/sub_type_renderer.py" line="63"/>
        <source>Graduated</source>
        <translation>Gradué</translation>
    </message>
</context>
<context>
    <name>TimeSeriesWrapper</name>
    <message>
        <location filename="../main_dialog.py" line="637"/>
        <source>Add a new column configuration</source>
        <translation>Ajouter une nouvelle configuration de colonne</translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="644"/>
        <source>Remove a column from configuration</source>
        <translation>Retirer une visualisation de la configuration</translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="722"/>
        <source>Impossible to add plot without selecting a feature</source>
        <translation>Impossible d&apos;ajouter une visualisation sans sélectionner une entité</translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="652"/>
        <source>Move up</source>
        <translation>Monter</translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="653"/>
        <source>Move down</source>
        <translation>Descendre</translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="654"/>
        <source>Edit style</source>
        <translation>Editer le style</translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="655"/>
        <source>Add a data cell</source>
        <translation>Afficher une visualisation</translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="656"/>
        <source>Remove a data cell</source>
        <translation>Cacher une visualisation</translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="715"/>
        <source>Time</source>
        <translation>Temps</translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="657"/>
        <source>Set y range</source>
        <translation>Définir l&apos;étendue en y</translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="658"/>
        <source>Reset y range</source>
        <translation>Réinitialiser l&apos;étendue en y</translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="659"/>
        <source>Display legend</source>
        <translation>Afficher la légende</translation>
    </message>
</context>
<context>
    <name>WellLogViewWrapper</name>
    <message>
        <location filename="../main_dialog.py" line="291"/>
        <source>Add a new column configuration</source>
        <translation>Ajouter une nouvelle configuration de colonne</translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="298"/>
        <source>Remove a column from configuration</source>
        <translation>Retirer une visualisation de la configuration</translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="485"/>
        <source>Impossible to add plot without selecting a feature</source>
        <translation>Impossible d&apos;ajouter une visualisation sans sélectionner une entité</translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="498"/>
        <source>Access method not implemented !</source>
        <translation>Méthode d&apos;accès non implémentée !</translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="306"/>
        <source>Move left</source>
        <translation>Placer à gauche</translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="307"/>
        <source>Move right</source>
        <translation>Placer à droite</translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="308"/>
        <source>Edit style</source>
        <translation>Editer le style</translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="309"/>
        <source>Add a data cell</source>
        <translation>Afficher une visualisation</translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="310"/>
        <source>Remove a data cell</source>
        <translation>Cacher une visualisation</translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="391"/>
        <source>Elevation</source>
        <translation>Altitude</translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="399"/>
        <source>Depth</source>
        <translation>Profondeur</translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="311"/>
        <source>Set y range</source>
        <translation>Définir l&apos;étendue en y</translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="312"/>
        <source>Reset y range</source>
        <translation>Réinitialiser l&apos;étendue en y</translation>
    </message>
    <message>
        <location filename="../main_dialog.py" line="313"/>
        <source>Display legend</source>
        <translation>Afficher la légende</translation>
    </message>
</context>
<context>
    <name>YManagementDialog</name>
    <message>
        <location filename="../qgeologis/y_management_dialog.py" line="42"/>
        <source>min_value</source>
        <translation>valeur_min</translation>
    </message>
    <message>
        <location filename="../qgeologis/y_management_dialog.py" line="50"/>
        <source>max_value</source>
        <translation>valeur_max</translation>
    </message>
</context>
</TS>
