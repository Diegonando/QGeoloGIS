# Changelog

This file is inspired from [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), this
project respects the [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

### Added

- Advanced symbology
- Legend action in dockwidget toolbar

### Modified

- Select the layer to modify in case of stacked items. (#89)
- Plot edit dialog does not trigger refresh if the dialog is rejected
- Y range management
- Polygon rendering base must be at 0 (bottom of the polygon if the data is positive, top of the polygon if the data is negative)

## v1.8.1 (2021-06-02)

### Added

- Enhance CI to publish plugin tag to plugins.qgis.org

### Modified


### Fixed

- fix cumulative log data with elevation scale
- fix tick when number with high precision

## v1.8.0 (2021-05-11)

### Added

- y axis tick
- point symbol for interval data
- specific activity conversion
- some units
- `.pylintrc` file for configuring Pylint runs. (#85)

### Modified

- optimize feature render to reduce loading time
- The minimum width of the QGeoloGIS panel is set to 300px (instead of 600px).
- use QgsDataSourceUri instead of splitting layer source
- The Pylint job within the CI fails under 8.5/10. (#85)

### Fixed

- stratigraphy form when the stratigraphy plot is not the first plot on the left
- loading saved point style for a plot
- fix polygon style for elevation scale plot

## v1.7.0  (2020-12-16)

### Added

- gpkg dataset

### Modified

### Fixed

- fix some unexpected bugs introduce by psycopg, especially to request feature form
- fix null data in uncertainty values (stacked data)
- fix feature data interface
- Allow to plot data that comes from layers with a geometry. (#81)

## v1.6.0 (2020-10-30)

### Added

- add depth information in the tooltip when altimetric scale is activated
- add support to trigger function on selection change
- translation for tooltip and scale column title

### Modified

- in config.py, stratigraphic and image plot can't be categorical data. In consequence, the display_cat key is not in the plot configuration
- some refactoring to optimize data loading and QGraphicsView refresh
- handle deprecated use of implicit int casting
- replace `getFeatures()` by `psycopg2` API in order to fasten data requests (#77)

### Fixed

- uncertainty column configuration in edit_plot dialog didn't work for categorical data
- fix auto scale when station only have stratigraphic data
- concentration units were forgotten in the configure plot dialog
- starting a new configuration from zero led to bugs because timeseries section were not initiated
- configuration export was broken by stacked data and has been fixed
- fix bad update of symbol legends after style update

## v1.5.0 (2020-08-28)

### Added

- Internationalization and french translation
- Units management
- Uncertainty visualisation
- Configuration of y axis ( logarithmic scale, min/max, displayed unit, plot width) by double clicking on
- Altimetric scale when a user mentions a ground altitude column in the configuration
- Fixed scale, movable column (pan on headers)
- Panning on y axis plot (pan on columns)
- Timeseries and log tabs are dissociated now
- Open QGIS feature form by clicking on a displayed element in plot
- Configuration allows to stack several data in a single cell if data share the same unit type.

### Modified

- Interval plot has been replaced by a interval data class that can be used by PlotItem.
- Configuration dialog changed to rationalizes layer types
- Items has been added to the configuration dialog to configure new features added in the current version.

### Fixed

- Categorical data has been fixed (broken in v1.4.0)
- Fix bisect troubles

## v1.4.0 (2020-07-06)

### Added

- `L*` rock code in `stratigraphy_style.xml`
- An hourglass cursor when loading features
- Interval plots
- Sample dataset (as a database dump, with a configuration generation script, and a QGIS project) (#56)
- Station names in column header
- New functionalities for interacting with the plot configuration (add/remove, display)
- A first documentation, with a tutorial for beginners (#61)

### Modified

- A bit of refactoring regarding data types (from *continuous* to *cumulative*)

### Fixed

- Touchpad control for horizontal plot (#21)
- `Nodata` value handling
- Style handling for timeseries
- Time scales for dates < 1970
- Ask for plot configuration only for layers with geometry
- Avoid the display of `NULL` formation codes for stratigraphy plot

## v1.3.1 (2020-03-20)

## v1.3.0 (2020-02-14)

## v1.2.0 (2020-01-21)

## v1.1.0 (2019-07-12)
