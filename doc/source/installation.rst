************
Installation
************

What is QGeoloGIS
-----------------

QGeoloGIS is a QGIS plugin designed to run under QGIS3. It is a light tool dedicated to geophysical measurement visualization.

How to install QGeoloGIS
------------------------

QGeoloGIS is installable by copying its source into the relevant QGIS plugin directory.

The process is automated through the ``Makefile``:

.. role:: bash(code)
	  :language: bash

.. code-block:: bash

		make deploy

