********
Tutorial
********

The images displayed in this tutorial come from the ``qgeologistest`` database, available in the ``sample`` folder.

Setup plugin
============

QGeoloGIS may be activated as a standard QGIS plugin (see ``Extensions/Manage extensions``).

.. image:: images/c06_load_qgeologis_plugin.png

QGeoloGIS makes two new icons available. They are related to either the depth view or the time view, depending on the type of measurement one wants to plot.

.. image:: images/c07_qgeologis_tools.png

QGeoloGIS implies the use of the QGIS selection tool, so as to select the station(s) where measures should be plotted.

.. image:: images/c08_select_tool.png

Configure QGeoloGIS
===================

First of all, QGeoloGIS comes with a layer configuration. It describes which layers to use within a project, and amongst them which layers to display by default. The configuration is closely related to the used database.

Setup configuration
-------------------

Basically, trying to open a plugin view with an unconfigured layer will open a dialog dedicated to the initialization of the layer configuration.

.. image:: images/c17_unconfigured_station_layer.png

In this dialog, the `id` and `name` columns must be specified, they correspond to the layer table. If your table has a elevation column, you can mention it with the `ground elevation column` parameter.

Generate a configuration from a script
--------------------------------------

In order to bridge the gap between the data stored in the database and the configured layers, which are exploited by the plugin, one may design a Python configuration generation script.

The following screenshots will guide the user during the configuration generation process, by depicting the ``qgeologistest`` example database:

- first open the Python console within QGIS:

.. image:: images/c01_python_console.png

- then open a Python script (for instance, ``sample/generate_config_test.py``), or write it (the sample script may be a source of inspiration...):

.. image:: images/c02_open_python_script.png

- and just run it:

.. image:: images/c03_run_script.png

- if everything is OK, some brand new layers (one for station, and the other ones for measurements) will appear in the layer panel:

.. image:: images/c04_layers_after_load.png

- starting from this point, QGeoloGIS may be used, and some symbology may be applied to the station layer:

.. image:: images/c05_stations_with_symbology.png

.. _import_export_configuration:

Import/export Configuration
---------------------------

As an alternative solution for setting the plugin configuration, one may directly load a ``.json`` configuration from the file system. This is a far more practical way of loading a configuration like those designed for the ``geologistest`` sample...

.. image:: images/c16_import_configuration.png

Alternatively, the configuration may be exported on the file system for further exploitation. Please note that it will be closely linked to the used database; hence a modification of this data source will trouble the future plugin usage.

.. image:: images/c15_export_configuration.png

Plot measures
=============

At this stage, the tutorial makes the assomption that the user has loaded a configuration, hence gets a station layer and some measurements layers.

The next section will be focused on the plugin viewers, available by clicking on the plugin icons:

.. image:: images/c07_qgeologis_tools.png

.. note::

   From here, the station layer must be selected so as to refer to the current configuration.

Log view
----------

The log view allows to visualize any measures expressed with respect to a depth. If a ground elevation column has been configured in the station layer, the log view will use a elevation scale. Otherwise a depth scale will be used. The scale is expressed in meters.

For the geological context, it may include stratigraphy, fracturation rates, or various drilling characteristic. This view supports imagery as well.

By selecting a station with the QGIS selection tool, then clicking on the log view icon, one can get available measurement on this station. In the ``qgeologistest`` example, every measurement is included into the plugin configuration, however the user may have designed a lighter configuration, with only a subset of measure tables.

.. image:: images/c10a_dummy2_logview.png

One may also select several station, in order to compare any measurement between two (or more) stations.

.. image:: images/c10b_dummy2_dummy4_logview.png

.. note::

   **Tip:** In QGIS, one may select additional object on the canvas by maintaining the ``Shift`` key when drawing the area of interest with the mouse.

Time view
---------

The *time* view in QGeoloGIS makes possible to visualize any timestamped event. That may include geology, hydrogeology, weather, chemical analysis, chimney releases, and so on.

The functioning imitates the *depth* view functioning. The user has to select a station (or more) with the QGIS selection tool, then to click onto the relevant icon. It displays every timestamped measure available for the current station(s).

.. image:: images/c09a_dummy2_timeview.png

QGeoloGIS supports histograms as well, if the database contains *continuous* measurements (defined by a start date and an end date).

.. image:: images/c09b_dummy2_dummy3_timeview.png

Finally, the user may also choose to depicts categorical measurements that comes from *the same* table. As an example from the ``qgeologistest`` database, one has a ``chemical_analysis`` table that includes several chemical elements:

.. image:: images/c09c_dummy1_categorical_timeview.png

QGeoloGIS functionalities
=========================

The view panels come with additional functionalities that aims at providing more control to the user; especially by interacting with the plugin configuration.

Here are the available actions:

- One may hide a measure type from the panel (without dropping it from the configuration). The measure stays hidden while it is not asked to display it again. The measure type must be selected before clicking on the removal button.

.. image:: images/c11_remove_cell.png

- One may display a configured measure type with the '+' button, by picking into the measure type available in the plugin configuration. As the previous functionality, the displayed state will be valid while it is not asked to display the measure again.

.. image:: images/c12a_add_cell.png

It will open a new dialog in which the user will have to choose the measure type he wants to plot.

.. image:: images/c12b_add_data_cell_view.png

At this stage, one may exploit the `sub selection` label to add a specific category if the chosen table needs it (*example:* a chemical analysis table, that contain a `chemical element` column).

.. image:: images/c12c_add_data_cell_view.png

- One may add a new measure type **into the configuration**, hence making the change permanent.

.. image:: images/c13a_add_column_configuration.png

This button will open a QGIS dialog where the user will have to configuration all the parameters associated to the measure, in order to add it into the plugin configuration. The `source` means the measure (respectively, the table from the database), the `type` means either ``Stratigraphic``, ``Instantaneous`` or ``Continuous``, and other fields refer to the table columns.

.. image:: images/c13b_add_column_configuration_view.png

- One may remove a configured measure type from the configuration or remove all configuration for the layer using the trash button.

.. image:: images/c18a_remove_configuration.png

This button will open a QGIS dialog where the user will select the measure type to remove. The user can also remove the global configuration by clicking on the ``Remove layer configuration``.

.. image:: images/c18b_remove_configuration.png

- Taking advantage of the plugin design, and the richness of QGIS symbologies, one may customize the plot style. The measure type that will be tuned must be selected beforehand.

.. image:: images/c14a_edit_style.png

This functionality open the QGIS symbology dialog, where the plot style may be enhanced. The style modifications are applied every time the corresponding measure type is plotted, whatever the selected station. They are stored in the plugin configuration, and may be saved on the file system for further exploitation as the rest of the configuration content (see :ref:`import_export_configuration`).

.. image:: images/c14b_edit_style.png

- If a cell is selected, the '+' button will be replaced by the stack button. It allow to stack several measures in a single cell if the measures share the same unit type. The following dialog will open :

.. image:: images/c15a_stack_measures.png

Available plot can be selected to be stacked in the current cell.

.. image:: images/c15b_stack_mesures.png


Plot navigation
===============

Plots are interactive. In this part will be described the available interactions :

- On mouse over, a tool tip will be displayed on the bottom of the log/time_series view. A black cross will indicate which feature is used to fill the tool tip.

.. image:: images/c16_tooltip.png

- If cells exceed the dock size, a drag and drop action is available on the cell headers.

- On cell the drag and drop action will move the plot on both axis.

- Wheel action is used to zoom in and out on the x axis (elevation or time axis). Combined with the Ctrl key, the wheel action is used to zoom on y axis (value axis).

- A single click on the cell will select the cell, providing specific features (hide plot button, stack cell button)

.. image:: images/c16a_selected_cell.png

- A single click on a displayed feature will open the feature form.

.. image:: images/c16b_feature_form.png

- A double click on the cell header will open the edit plot dialog :

.. image:: images/c17_edit_plot_dialog.png

This dialog is used to configure plot :

	- enable/disable logarithmic scale

	- define displayed unit

	- define min/max value

	- configure uncertainty column

	- configure plot size and also title size